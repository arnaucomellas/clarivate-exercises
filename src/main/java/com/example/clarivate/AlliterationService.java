package com.example.clarivate;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class AlliterationService {

    /**
     * Method that given a text and a letter, returns the percentage of words with the initial letter in the text
     * @return percentage as String
     */
    public Double calculatePercentageByLetter(String text, String letter) {
        Map<String, Double> map = getMapLettersCount(text);
        if (!map.isEmpty()) {
            Double countLetter = map.get(letter.toUpperCase());
            Double total = map.get("total");
            return (countLetter/total)*100.0;
        }
        else return 0d;
    }

    /**
     * Method that given a text and a letter, returns the number of words with the initial letter in the text
     * @return number
     */
    public Integer getNumberOfWordsByLetter(String text, String letter) {
        Map<String, Double> map = getMapLettersCount(text);
        if (!map.isEmpty()) return map.get(letter.toUpperCase()).intValue();
        else return 0;
    }

    /**
     * Method that given a text returns a map with the initials as Key and the number of words that the text has with the letter as initial
     * @return map
     */
    private Map<String, Double> getMapLettersCount(String text) {
        Map<String, Double> map = new HashMap<>();
        Pattern p = Pattern.compile("[a-zA-Z]+");
        Matcher m = p.matcher(text);
        double total = 0d;
        while (m.find()) {
            String initial = m.group().substring(0,1).toUpperCase();
            if (map.containsKey(initial)) {
                map.put(initial, map.get(initial)+1);
            } else map.put(initial, 1d);
            map.put("total", ++total);
        }
        return map;
    }
}
