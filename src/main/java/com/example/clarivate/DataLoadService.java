package com.example.clarivate;

import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class DataLoadService {

    /**
     * Method that given a list of integers representing the time of execution of consecutive
     * tasks, returns true if its is possible for the load balancer to choose two requests that will determine an
     * even distribution of requests among the three workers, or false otherwise.
     * The method uses two loops to decide the requests chosen by the load balancer.
     * If the sum of the remaining requests times is multiple of 3, the function distribute is called.
     * @param list
     * @return boolean
     */
    public boolean isDistributionPossible(List<Integer> list) {
        System.out.println("Start calculation for the list: "+list);
        if (list != null && list.size() >= 5) {
            int size = list.size();
            // totalSum, cost = size
            int totalSum = list.stream().reduce(0, Integer::sum);
            // sort list in descending, cost = size
            Collections.sort(list, Collections.reverseOrder());
            // Two loops that costs size * size/2 (worst case)
            for (int i = 0; i < size; ++i) {
                for (int j = i+1; j < size; ++j) {
                    int sum = totalSum - list.get(i) - list.get(j);
                    System.out.println("Load balancer choose values "+list.get(i)+", "+list.get(j)+". Sum of remaining: "+sum);
                    if (sum % 3 == 0 // If the sum is not multiple of 3, is not necessary calculate the distribution
                            && distribute(list, size, i, j, sum/3)) return true;
                }
            }
        }
        return false;
    }

    /**
     * Given a list of request times sorted in descending order and three workers
     * We fill one worker at a time.
     * The requests are processed by decreasing time.
     * For each request: if it fits the current worker, it is assigned to it, otherwise, the next request is processed
     * Once a worker is full to the exact capacity we start filling the next worker, again, we start processing the largest remaining request
     * If a worker has not been filled once all requests have been considered, the distribution is not possible.
     * In such case, we stop processing any further
     */
    private boolean distribute(List<Integer> list, int size, int i, int j, int maxByWorker) {
        boolean[] used = new boolean[size];
        //cost = 3n (worst case)
        for (int worker = 1; worker <= 3; ++worker) {
            int workerSum = 0;
            for (int k = 0; k < size; ++k) {
                if (k != i && k != j && !used[k])
                    if (list.get(k) > maxByWorker) return false;
                    else if (workerSum + list.get(k) <= maxByWorker) {
                        System.out.println("Put " + list.get(k) + " at worker " + worker);
                        workerSum += list.get(k);
                        used[k] = true;
                    }
                if(workerSum == maxByWorker) break;
            }
            if (workerSum != maxByWorker) {
                System.out.println("Can't complete worker " + worker);
                return false;
            }
            System.out.println("Worker " + worker + " has been distributed correctly");
        }
        System.out.println("The distribution is possible");
        return true;
    }
}
