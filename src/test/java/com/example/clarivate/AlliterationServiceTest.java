package com.example.clarivate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
public class AlliterationServiceTest {

    @Autowired
    private AlliterationService alliterationService;

    @Test
    public void getNumberOfWordsTest() throws Exception {
        String text1 = "Mike made mellow music with his new microphone.";
        String text2 = "Yarvis yanked his ankle at yoga, and Yolanda yelled out in surprise.";
        assertThat(alliterationService.getNumberOfWordsByLetter(text1, "m")).isEqualTo(5);
        assertThat(alliterationService.getNumberOfWordsByLetter(text1, "n")).isEqualTo(1);
        assertThat(alliterationService.getNumberOfWordsByLetter(text2, "y")).isEqualTo(5);
        assertThat(alliterationService.getNumberOfWordsByLetter(text2, "a")).isEqualTo(3);
    }

    @Test
    public void getPercentageOfWordsTest() {
        String text1 = "Mike made mellow music with his new microphone.";
        String text2 = "Yarvis yanked his ankle at yoga, and Yolanda yelled out in surprise.";
        assertThat(alliterationService.calculatePercentageByLetter(text1, "m")).isEqualTo(62.5);
        assertThat(alliterationService.calculatePercentageByLetter(text2, "a")).isEqualTo(25);
    }
}
