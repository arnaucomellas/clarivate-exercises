package com.example.clarivate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureMockMvc
public class DataLoadServiceTest {

    @Autowired
    private DataLoadService dataLoadService;

    @Test
    public void test1() throws Exception {
        List<Integer> list = Arrays.asList(1,3,4,2,2,2,1,1,2);
        assertThat(dataLoadService.isDistributionPossible(list)).isTrue();
    }

    @Test
    public void test2() throws Exception {
        List<Integer> list = Arrays.asList(1,1,1,1,1,1);
        assertThat(dataLoadService.isDistributionPossible(list)).isFalse();
    }

    @Test
    public void test3() throws  Exception {
        List<Integer> list = Arrays.asList(5, 4, 1, 2, 3, 6, 7);
        assertThat(dataLoadService.isDistributionPossible(list)).isTrue();
        List<Integer> list2 = Arrays.asList(6, 4, 1, 2, 3, 6, 7);
        assertThat(dataLoadService.isDistributionPossible(list2)).isTrue();
        List<Integer> list3 = Arrays.asList(2,2,3,2,100,100);
        assertThat(dataLoadService.isDistributionPossible(list3)).isFalse();
        List<Integer> list4 = Arrays.asList(4,4,4,5,5);
        assertThat(dataLoadService.isDistributionPossible(list4)).isTrue();
        List<Integer> list5 = Arrays.asList(5,5,10,2,2,1,1,1,3,6,1);
        assertThat(dataLoadService.isDistributionPossible(list5)).isTrue();
    }
}
